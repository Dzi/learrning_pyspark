{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Binary Classification Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Pipelines API provides higher-level API built on top of DataFrames for constructing ML pipelines.\n",
    "You can read more about the Pipelines API in the [programming guide](https://spark.apache.org/docs/latest/ml-guide.html).\n",
    "\n",
    "**Binary Classification** is the task of predicting a binary label.\n",
    "E.g., is an email spam or not spam? Should I show this ad to this user or not? Will it rain tomorrow or not?\n",
    "This section demonstrates algorithms for making these types of predictions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dataset Review"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Adult dataset we are going to use is publicly available at the [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Adult).\n",
    "This data derives from census data, and consists of information about 48842 individuals and their annual income.\n",
    "We will use this information to predict if an individual earns >50k a year or <=50K a year.\n",
    "The dataset is rather clean, and consists of both numeric and categorical variables.\n",
    "\n",
    "Attribute Information:\n",
    "\n",
    "- age: continuous\n",
    "- workclass: Private,Self-emp-not-inc, Self-emp-inc, Federal-gov, Local-gov, State-gov, Without-pay, Never-worked\n",
    "- fnlwgt: continuous\n",
    "- education: Bachelors, Some-college, 11th, HS-grad, Prof-school, Assoc-acdm, Assoc-voc...\n",
    "- education-num: continuous\n",
    "- marital-status: Married-civ-spouse, Divorced, Never-married, Separated, Widowed, Married-spouse-absent...\n",
    "- occupation: Tech-support, Craft-repair, Other-service, Sales, Exec-managerial, Prof-specialty, Handlers-cleaners...\n",
    "- relationship: Wife, Own-child, Husband, Not-in-family, Other-relative, Unmarried\n",
    "- race: White, Asian-Pac-Islander, Amer-Indian-Eskimo, Other, Black\n",
    "- sex: Female, Male\n",
    "- capital-gain: continuous\n",
    "- capital-loss: continuous\n",
    "- hours-per-week: continuous\n",
    "- native-country: United-States, Cambodia, England, Puerto-Rico, Canada, Germany...\n",
    "\n",
    "Target/Label: - <=50K, >50K"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, we will read in the Adult dataset from databricks-datasets.\n",
    "We'll read in the data in SQL using the CSV data source for Spark and rename the columns appropriately."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ls adult_dataset/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "COLNAMES = ['age', 'workclass', 'fnlwgt', 'education', 'education-num', 'marital-status', 'occupation',\n",
    "            'relationship', 'race', 'sex', 'capital-gain', 'capital-loss', 'hours-per-week', 'native-country',\n",
    "            'income']\n",
    "\n",
    "\n",
    "def rename_columns(df, columns):\n",
    "    if isinstance(columns, dict):\n",
    "        for old_name, new_name in columns.items():\n",
    "            df = df.withColumnRenamed(old_name, new_name)\n",
    "        return df\n",
    "    else:\n",
    "        raise ValueError(\"'columns' should be a dict, like {'old_name_1':'new_name_1', 'old_name_2':'new_name_2'}\")\n",
    "        \n",
    "\n",
    "def get_dataframe(path):\n",
    "    df = spark.read.csv(path, header=False, inferSchema=True, nanValue='?')\n",
    "    colname_map = { '_c{}'.format(i): COLNAMES[i] for i in range(len(COLNAMES))}\n",
    "    return rename_columns(df, colname_map)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark import SparkContext\n",
    "from pyspark.sql import SparkSession, SQLContext\n",
    "from pyspark.sql.types import StructType, StructField, IntegerType, StringType\n",
    "import atexit\n",
    "\n",
    "\n",
    "sc = SparkContext()\n",
    "sql_context = SQLContext(sc)\n",
    "\n",
    "# Stop the Spark context when the program terminates\n",
    "atexit.register(lambda: sc.stop())\n",
    "\n",
    "spark = (SparkSession\n",
    "         .builder\n",
    "         .getOrCreate())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_df = get_dataframe('adult_dataset/adult.data')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display(train_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_df.limit(10).toPandas()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preprocess Data\n",
    "\n",
    "Since we are going to try algorithms like Logistic Regression, we will have to convert the categorical variables in the dataset into numeric variables.\n",
    "There are 2 ways we can do this.\n",
    "\n",
    "* Category Indexing\n",
    "\n",
    "  This is basically assigning a numeric value to each category from {0, 1, 2, ...numCategories-1}.\n",
    "  This introduces an implicit ordering among your categories, and is more suitable for ordinal variables (eg: Poor: 0, Average: 1, Good: 2)\n",
    "\n",
    "* One-Hot Encoding\n",
    "\n",
    "  This converts categories into binary vectors with at most one nonzero value (eg: (Blue: [1, 0]), (Green: [0, 1]), (Red: [0, 0]))\n",
    "\n",
    "In this dataset, we have ordinal variables like education (Preschool - Doctorate), and also nominal variables like relationship (Wife, Husband, Own-child, etc).\n",
    "For simplicity's sake, we will use One-Hot Encoding to convert all categorical variables into binary vectors.\n",
    "It is possible here to improve prediction accuracy by converting each categorical column with an appropriate method.\n",
    "\n",
    "Here, we will use a combination of [StringIndexer] and [OneHotEncoderEstimator] to convert the categorical variables.\n",
    "The `OneHotEncoderEstimator` will return a [SparseVector].\n",
    "\n",
    "Since we will have more than 1 stages of feature transformations, we use a [Pipeline] to tie the stages together.\n",
    "This simplifies our code.\n",
    "\n",
    "[StringIndexer]: http://spark.apache.org/docs/latest/ml-features.html#stringindexer\n",
    "[OneHotEncoderEstimator]: https://spark.apache.org/docs/latest/ml-features.html#onehotencoderestimator\n",
    "[SparseVector]: https://spark.apache.org/docs/latest/api/python/pyspark.ml.html#pyspark.ml.linalg.SparseVector\n",
    "[Pipeline]: http://spark.apache.org/docs/latest/api/python/pyspark.ml.html#pyspark.ml.Pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml import Pipeline\n",
    "from pyspark.ml.feature import OneHotEncoderEstimator, StringIndexer, VectorAssembler\n",
    "\n",
    "\n",
    "categoricalColumns = [\"workclass\", \"education\", \"marital-status\", \"occupation\", \"relationship\", \"race\", \"sex\", \"native-country\"]\n",
    "stages = [] # stages in our Pipeline\n",
    "for categoricalCol in categoricalColumns:\n",
    "    # Category Indexing with StringIndexer\n",
    "    stringIndexer = StringIndexer(inputCol=categoricalCol, outputCol=categoricalCol + \"Index\")\n",
    "    \n",
    "    # Use OneHotEncoder to convert categorical variables into binary SparseVectors\n",
    "    encoder = OneHotEncoderEstimator(inputCols=[stringIndexer.getOutputCol()], outputCols=[categoricalCol + \"classVec\"])\n",
    "    \n",
    "    # Add stages.  These are not run here, but will run all at once later on.\n",
    "    stages += [stringIndexer, encoder]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above code basically indexes each categorical column using the `StringIndexer`,\n",
    "and then converts the indexed categories into one-hot encoded variables.\n",
    "The resulting output has the binary vectors appended to the end of each row.\n",
    "\n",
    "We use the `StringIndexer` again to encode our labels to label indices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert label into label indices using the StringIndexer\n",
    "label_stringIdx = StringIndexer(inputCol=\"income\", outputCol=\"label\")\n",
    "stages += [label_stringIdx]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will use the `VectorAssembler` to combine all the feature columns into a single vector column.\n",
    "This will include both the numeric columns and the one-hot encoded binary vector columns in our dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Transform all features into a vector using VectorAssembler\n",
    "numericCols = [\"age\", \"fnlwgt\", \"education-num\", \"capital-gain\", \"capital-loss\", \"hours-per-week\"]\n",
    "assemblerInputs = [c + \"classVec\" for c in categoricalColumns] + numericCols\n",
    "assembler = VectorAssembler(inputCols=assemblerInputs, outputCol=\"features\")\n",
    "stages += [assembler]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We finally run our stages as a Pipeline.\n",
    "This puts the data through all of the feature transformations we described in a single call."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Pipeline.\n",
    "pipeline = Pipeline(stages=stages)\n",
    "\n",
    "# Run the feature transformations.\n",
    "#  - fit() computes feature statistics as needed.\n",
    "#  - transform() actually transforms the features.\n",
    "pipelineModel = pipeline.fit(train_df)\n",
    "train_df = pipelineModel.transform(train_df)\n",
    "\n",
    "# Keep relevant columns\n",
    "selectedcols = [\"label\", \"features\"] + COLNAMES\n",
    "train_df = train_df.select(selectedcols)\n",
    "# display(train_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "train_df.limit(10).toPandas()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_df = get_dataframe('adult_dataset/adult.test')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark import keyword_only\n",
    "from pyspark.ml import Transformer\n",
    "from pyspark.ml.param.shared import HasInputCol, HasOutputCol, Param\n",
    "from pyspark.sql.functions import udf\n",
    "from pyspark.sql.types import ArrayType, StringType\n",
    "\n",
    "\n",
    "\n",
    "class DeleteTrailingDotTransformer(Transformer, HasInputCol, HasOutputCol):\n",
    "    '''An example of a custom Transformer.\n",
    "    \n",
    "    A more sophisticated example:\n",
    "        https://stackoverflow.com/questions/32331848/create-a-custom-transformer-in-pyspark-ml\n",
    "    '''\n",
    "\n",
    "    @keyword_only\n",
    "    def __init__(self, inputCol=None, outputCol=None):\n",
    "        super(DeleteTrailingDotTransformer, self).__init__()\n",
    "        kwargs = self._input_kwargs\n",
    "#         self.setParams(**kwargs)\n",
    "        self._set(**kwargs)\n",
    "\n",
    "#     @keyword_only\n",
    "#     def setParams(self, inputCol=None, outputCol=None):\n",
    "#         kwargs = self._input_kwargs\n",
    "#         return self._set(**kwargs)\n",
    "\n",
    "    def _transform(self, dataset):\n",
    "        def f(s):\n",
    "            return s.replace('.', '')\n",
    "\n",
    "        out_col = self.getOutputCol()\n",
    "        in_col = dataset[self.getInputCol()]\n",
    "        return dataset.withColumn(out_col, udf(f, StringType())(in_col))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transformer = DeleteTrailingDotTransformer(inputCol='income', outputCol='income')\n",
    "test_df = transformer.transform(test_df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fit and Evaluate Models\n",
    "\n",
    "We are now ready to try out some of the Binary Classification algorithms available in the Pipelines API.\n",
    "\n",
    "Out of these algorithms, the below are also capable of supporting multiclass classification with the Python API:\n",
    "- Decision Tree Classifier\n",
    "- Random Forest Classifier\n",
    "\n",
    "These are the general steps we will take to build our models:\n",
    "- Create initial model using the training set\n",
    "- Tune parameters with a `ParamGrid` and 5-fold Cross Validation\n",
    "- Evaluate the best model obtained from the Cross Validation using the test set\n",
    "\n",
    "We use the `BinaryClassificationEvaluator` to evaluate our models, which uses [areaUnderROC] as the default metric.\n",
    "\n",
    "[areaUnderROC]: https://en.wikipedia.org/wiki/Receiver_operating_characteristic#Area_under_the_curve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Logistic Regression\n",
    "\n",
    "You can read more about [Logistic Regression] from the [classification and regression] section of MLlib Programming Guide.\n",
    "In the Pipelines API, we are now able to perform Elastic-Net Regularization with Logistic Regression, as well as other linear methods.\n",
    "\n",
    "[classification and regression]: https://spark.apache.org/docs/latest/ml-classification-regression.html\n",
    "[Logistic Regression]: https://spark.apache.org/docs/latest/ml-classification-regression.html#logistic-regression"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml.classification import LogisticRegression\n",
    "\n",
    "\n",
    "# Create initial LogisticRegression model\n",
    "lr = LogisticRegression(labelCol=\"label\", featuresCol=\"features\", maxIter=10)\n",
    "\n",
    "# Train model with Training Data\n",
    "lrModel = lr.fit(train_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_df = pipelineModel.transform(test_df)\n",
    "\n",
    "# Make predictions on test data using the transform() method.\n",
    "# LogisticRegression.transform() will only use the 'features' column.\n",
    "predictions = lrModel.transform(test_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "predictions.printSchema()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View model's predictions and probabilities of each prediction class\n",
    "# You can select any columns in the above schema to view as well. For example's sake we will choose age & occupation\n",
    "selected = predictions.select(\"label\", \"prediction\", \"probability\", \"age\", \"occupation\")\n",
    "display(selected)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use ``BinaryClassificationEvaluator`` to evaluate our model. We can set the required column names in `rawPredictionCol` and `labelCol` Param and the metric in `metricName` Param."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml.evaluation import BinaryClassificationEvaluator\n",
    "\n",
    "\n",
    "# Evaluate model\n",
    "evaluator = BinaryClassificationEvaluator(rawPredictionCol=\"rawPrediction\")\n",
    "evaluator.evaluate(predictions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the default metric for the ``BinaryClassificationEvaluator`` is ``areaUnderROC``"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "evaluator.getMetricName()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The evaluator currently accepts 2 kinds of metrics - areaUnderROC and areaUnderPR.\n",
    "We can set it to areaUnderPR by using evaluator.setMetricName(\"areaUnderPR\").\n",
    "\n",
    "Now we will try tuning the model with the ``ParamGridBuilder`` and the ``CrossValidator``.\n",
    "\n",
    "If you are unsure what params are available for tuning, you can use ``explainParams()`` to print a list of all params and their definitions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(lr.explainParams())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we indicate 3 values for regParam, 3 values for maxIter, and 2 values for elasticNetParam,\n",
    "this grid will have 3 x 3 x 3 = 27 parameter settings for CrossValidator to choose from.\n",
    "We will create a 5-fold cross validator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml.tuning import ParamGridBuilder, CrossValidator\n",
    "\n",
    "\n",
    "# Create ParamGrid for Cross Validation\n",
    "paramGrid = (ParamGridBuilder()\n",
    "             .addGrid(lr.regParam, [0.01, 0.5, 2.0])\n",
    "             .addGrid(lr.elasticNetParam, [0.0, 0.5, 1.0])\n",
    "             .addGrid(lr.maxIter, [1, 5, 10])\n",
    "             .build())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create 5-fold CrossValidator\n",
    "cv = CrossValidator(estimator=lr, estimatorParamMaps=paramGrid, evaluator=evaluator, numFolds=5)\n",
    "\n",
    "# Run cross validations\n",
    "cvModel = cv.fit(train_df)\n",
    "# this will likely take a fair amount of time because of the amount of models that we're creating and testing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use test set to measure the accuracy of our model on new data\n",
    "predictions = cvModel.transform(test_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cvModel uses the best model found from the Cross Validation\n",
    "# Evaluate best model\n",
    "evaluator.evaluate(predictions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also access the model's feature weights and intercepts easily"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Model Intercept: ', cvModel.bestModel.intercept)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "weights = cvModel.bestModel.coefficients\n",
    "weights = [(float(w),) for w in weights]  # convert numpy type to float, and to tuple\n",
    "weights_df = sql_context.createDataFrame(weights, [\"Feature Weight\"])\n",
    "display(weights_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View best model's predictions and probabilities of each prediction class\n",
    "selected = predictions.select(\"label\", \"prediction\", \"probability\", \"age\", \"occupation\")\n",
    "display(selected)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Decision Trees\n",
    "\n",
    "You can read more about [Decision Trees](http://spark.apache.org/docs/latest/mllib-decision-tree.html) in the Spark MLLib Programming Guide.\n",
    "The Decision Trees algorithm is popular because it handles categorical\n",
    "data and works out of the box with multiclass classification tasks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml.classification import DecisionTreeClassifier\n",
    "\n",
    "\n",
    "# Create initial Decision Tree Model\n",
    "dt = DecisionTreeClassifier(labelCol=\"label\", featuresCol=\"features\", maxDepth=3)\n",
    "\n",
    "# Train model with Training Data\n",
    "dtModel = dt.fit(train_df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can extract the number of nodes in our decision tree as well as the\n",
    "tree depth of our model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"numNodes = \", dtModel.numNodes)\n",
    "print(\"depth = \", dtModel.depth)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make predictions on test data using the Transformer.transform() method.\n",
    "predictions = dtModel.transform(test_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View model's predictions and probabilities of each prediction class\n",
    "selected = predictions.select(\"label\", \"prediction\", \"probability\", \"age\", \"occupation\")\n",
    "display(selected)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will evaluate our Decision Tree model with\n",
    "`BinaryClassificationEvaluator`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml.evaluation import BinaryClassificationEvaluator\n",
    "\n",
    "\n",
    "# Evaluate model\n",
    "evaluator = BinaryClassificationEvaluator()\n",
    "evaluator.evaluate(predictions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Entropy and the Gini coefficient are the supported measures of impurity for Decision Trees. This is ``Gini`` by default. Changing this value is simple, ``model.setImpurity(\"Entropy\")``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dt.getImpurity()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will try tuning the model with the ``ParamGridBuilder`` and the ``CrossValidator``.\n",
    "\n",
    "As we indicate 3 values for maxDepth and 3 values for maxBin, this grid will have 3 x 3 = 9 parameter settings for ``CrossValidator`` to choose from. We will create a 5-fold CrossValidator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create ParamGrid for Cross Validation\n",
    "from pyspark.ml.tuning import ParamGridBuilder, CrossValidator\n",
    "\n",
    "\n",
    "paramGrid = (ParamGridBuilder()\n",
    "             .addGrid(dt.maxDepth, [1, 2, 6, 10])\n",
    "             .addGrid(dt.maxBins, [20, 40, 80])\n",
    "             .build())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create 5-fold CrossValidator\n",
    "cv = CrossValidator(estimator=dt, estimatorParamMaps=paramGrid, evaluator=evaluator, numFolds=5)\n",
    "\n",
    "# Run cross validations\n",
    "cvModel = cv.fit(train_df)\n",
    "# Takes ~5 minutes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"numNodes = \", cvModel.bestModel.numNodes)\n",
    "print(\"depth = \", cvModel.bestModel.depth)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use test set to measure the accuracy of our model on new data\n",
    "predictions = cvModel.transform(test_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cvModel uses the best model found from the Cross Validation\n",
    "# Evaluate best model\n",
    "evaluator.evaluate(predictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View Best model's predictions and probabilities of each prediction class\n",
    "selected = predictions.select(\"label\", \"prediction\", \"probability\", \"age\", \"occupation\")\n",
    "display(selected)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Random Forest\n",
    "\n",
    "Random Forests uses an ensemble of trees to improve model accuracy.\n",
    "You can read more about [Random Forest] from the [classification and regression] section of MLlib Programming Guide.\n",
    "\n",
    "[classification and regression]: https://spark.apache.org/docs/latest/ml-classification-regression.html\n",
    "[Random Forest]: https://spark.apache.org/docs/latest/ml-classification-regression.html#random-forests"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml.classification import RandomForestClassifier\n",
    "\n",
    "\n",
    "# Create an initial RandomForest model.\n",
    "rf = RandomForestClassifier(labelCol=\"label\", featuresCol=\"features\")\n",
    "\n",
    "\n",
    "# Train model with Training Data\n",
    "rfModel = rf.fit(train_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make predictions on test data using the Transformer.transform() method.\n",
    "predictions = rfModel.transform(test_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View model's predictions and probabilities of each prediction class\n",
    "selected = predictions.select(\"label\", \"prediction\", \"probability\", \"age\", \"occupation\")\n",
    "display(selected)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will evaluate our Random Forest model with `BinaryClassificationEvaluator`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.ml.evaluation import BinaryClassificationEvaluator\n",
    "\n",
    "# Evaluate model\n",
    "evaluator = BinaryClassificationEvaluator()\n",
    "evaluator.evaluate(predictions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will try tuning the model with the ``ParamGridBuilder`` and the ``CrossValidator``.\n",
    "\n",
    "As we indicate 3 values for maxDepth, 2 values for maxBin, and 2 values for numTrees,\n",
    "this grid will have 3 x 2 x 2 = 12 parameter settings for ``CrossValidator`` to choose from.\n",
    "We will create a 5-fold ``CrossValidator``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create ParamGrid for Cross Validation\n",
    "from pyspark.ml.tuning import ParamGridBuilder, CrossValidator\n",
    "\n",
    "paramGrid = (ParamGridBuilder()\n",
    "             .addGrid(rf.maxDepth, [2, 4, 6])\n",
    "             .addGrid(rf.maxBins, [20, 60])\n",
    "             .addGrid(rf.numTrees, [5, 20])\n",
    "             .build())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create 5-fold CrossValidator\n",
    "cv = CrossValidator(estimator=rf, estimatorParamMaps=paramGrid, evaluator=evaluator, numFolds=5)\n",
    "\n",
    "# Run cross validations.  This can take about 6 minutes since it is training over 20 trees!\n",
    "cvModel = cv.fit(train_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use test set here so we can measure the accuracy of our model on new data\n",
    "predictions = cvModel.transform(test_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cvModel uses the best model found from the Cross Validation\n",
    "# Evaluate best model\n",
    "evaluator.evaluate(predictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View Best model's predictions and probabilities of each prediction class\n",
    "selected = predictions.select(\"label\", \"prediction\", \"probability\", \"age\", \"occupation\")\n",
    "display(selected)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Make Predictions\n",
    "As Random Forest gives us the best areaUnderROC value, we will use the bestModel obtained from Random Forest for deployment,\n",
    "and use it to generate predictions on new data.\n",
    "In this example, we will simulate this by generating predictions on the entire dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bestModel = cvModel.bestModel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate predictions for entire dataset\n",
    "finalPredictions = bestModel.transform(test_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Evaluate best model\n",
    "evaluator.evaluate(finalPredictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  },
  "name": "binary-classification",
  "notebookId": 1973453949697248
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
