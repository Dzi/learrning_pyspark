{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# RDD -- Resilient Distributed Dataset\n",
    "\n",
    "RDDs are immutable collections of data distributed among the nodes in a cluster.\n",
    "\n",
    "* **Resilient**, i.e. fault-tolerant (allows Spark to recompute missing or damaged partitions due to node failures);\n",
    "\n",
    "* **Distributed** (data residing on multiple nodes in a cluster);\n",
    "\n",
    "* **Dataset** (collection of partitioned data, e.g. tuples or other objects that represent records of the data you work with).\n",
    "\n",
    "\n",
    "\n",
    "From the original paper about RDD - [Resilient Distributed Datasets: A Fault-Tolerant Abstraction for In-Memory Cluster Computing](https://cs.stanford.edu/~matei/papers/2012/nsdi_spark.pdf):\n",
    "\n",
    "    Resilient Distributed Datasets (RDDs) are a distributed memory abstraction that lets programmers perform in-memory computations on large clusters in a fault-tolerant manner."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "There are two ways to create RDDs: parallelizing an existing collection in your driver program, or referencing a dataset in an external storage system, such as a shared filesystem, HDFS, HBase, or any data source offering a Hadoop InputFormat."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "RDDs have two sets of parallel operations: **transformations** (which return pointers to new RDDs) and **actions** (which return values to the driver after running a computation).\n",
    "\n",
    "**Actions** execute the computational graph specified by the trnasformations. They are operations that return something that isn’t an RDD and transformations that return another RDD.\n",
    "\n",
    "RDD **transformation** operations are **lazy** in a sense that they do not compute their results immediately. (There are exceptions, however.) The transformations are only computed when an action is executed and the results need to be returned to the driver. This delayed execution results in more fine-tuned queries: Queries that are optimized for performance. This optimization starts with Apache Spark's DAGScheduler -- the stage oriented scheduler that transforms using stages as seen in the preceding screenshot. By having separate RDD transformations and actions, the DAGScheduler can perform optimizations in the query including being able to avoid shuffling, the data (the most resource intensive task).\n",
    "\n",
    "Read more about [DAGScheduler](https://jaceklaskowski.gitbooks.io/mastering-apache-spark/spark-dagscheduler.html). Also, it's worth reading [the scaladoc for the DAGScheduler class ](https://github.com/apache/spark/blob/master/core/src/main/scala/org/apache/spark/scheduler/DAGScheduler.scala)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Examples\n",
    "\n",
    "Of transformations (bold methods are considered **expensive** -- use them wisely):\n",
    "\n",
    "`.map`, `.flatMap`, `.filter`, **`.distinct`**, `.sample`, **`.leftOuterJoin`**, **`.join`**, `.intersection`, **`.repartition`**.\n",
    "\n",
    "---\n",
    "\n",
    "Of actions:\n",
    "\n",
    "`.take`, `.takeSample`, `.count`, `.reduce`, `.reduceByKey`, **`.groupByKey`**, `.collect`, `.saveAsTextFile`, `.foreach`.\n",
    "\n",
    "---\n",
    "\n",
    "Interestingly, not all transformations are 100% lazy. `.sortByKey` (and `.sortBy`) needs to evaluate the RDD to determine the range of data, so it involves both a transformation and an action."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "from pyspark import SparkContext\n",
    "import atexit\n",
    "\n",
    "\n",
    "sc = SparkContext(appName='2-RDD')\n",
    "atexit.register(lambda: sc.close())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "sc.uiWebUrl"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "words_RDD = sc.parallelize(['cat', 'elephant', 'rat', 'rat', 'cat'], 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def make_plural(word):\n",
    "    return word + 's'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "plural_RDD = words_RDD.map(make_plural)\n",
    "plural_RDD.collect()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "word_pairs = words_RDD.map(lambda x: (x, 1))\n",
    "word_pairs.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "## [`groupByKey()`](http://spark.apache.org/docs/latest/api/python/pyspark.html#pyspark.RDD.groupByKey) and why it's slow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "There are two problems with using `groupByKey()`:\n",
    "  + The operation requires a lot of data movement to move all the values into the appropriate partitions.\n",
    "  + The lists can be very large. Consider a word count of English Wikipedia: the lists for common words (e.g., the, a, etc.) would be huge and could exhaust the available memory in a worker.\n",
    "  \n",
    "Read more [here.](https://databricks.gitbooks.io/databricks-spark-knowledge-base/content/best_practices/prefer_reducebykey_over_groupbykey.html) Also, if you'd like to read more about shuffling, check out [this](https://jaceklaskowski.gitbooks.io/mastering-apache-spark/spark-rdd-shuffle.html) section.\n",
    "\n",
    "Use `groupByKey()` to generate a pair RDD of type `('word', iterator)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "words_grouped = word_pairs.groupByKey()\n",
    "for key, value in words_grouped.collect():\n",
    "    print('{0}: {1}'.format(key, list(value)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "word_counts_grouped = words_grouped.map(\n",
    "    lambda key_value: (key_value[0], sum(key_value[1])))\n",
    "print(word_counts_grouped.collect())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Counting using `reduceByKey()`\n",
    "\n",
    "A better approach is to start from the pair RDD and then use the [reduceByKey()](http://spark.apache.org/docs/latest/api/python/pyspark.html#pyspark.RDD.reduceByKey) transformation to create a new pair RDD. The `reduceByKey()` transformation gathers together pairs that have the same key and applies the function provided to two values at a time, iteratively reducing all of the values to a single value. `reduceByKey()` operates by applying the function first within each partition on a per-key basis and then across the partitions, allowing it to scale efficiently to large datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "word_counts = word_pairs.reduceByKey(lambda x, y: x+y)\n",
    "print(word_counts.collect())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A word of caution is necessary here. The functions passed as a reducer need to be associative, that is, when the order of elements is changed the result does not, and commutative, that is, changing the order of operands does not change the result either.\n",
    "\n",
    "The example of the associativity rule is (5 + 2) + 3 = 5 + (2 + 3), and of the commutative is 5 + 2 + 3 = 3 + 2 + 5. Thus, you need to be careful about what functions you pass to the reducer.\n",
    "\n",
    "If you ignore the preceding rule, you might run into trouble (assuming your code runs at all). For example, let's assume we have the following RDD (with one partition only!):"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Putting it all together"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "(words_RDD\n",
    " .map(lambda x: (x, 1))\n",
    " .reduceByKey(lambda x, y: x+y)\n",
    " .collect())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or, quicker:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "words_RDD.countByKey()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "By the way, this is how it looks in Scala:\n",
    "\n",
    "```scala\n",
    "val words = Array(\"cat\", \"elephant\", \"rat\", \"rat\", \"cat\")\n",
    "val wordPairs = sc.parallelize(words).map(word => (word, 1))\n",
    "\n",
    "val wordCountsWithReduce = wordPairsRDD\n",
    "  .reduceByKey(_ + _)\n",
    "  .collect()\n",
    "\n",
    "val wordCountsWithGroup = wordPairsRDD\n",
    "  .groupByKey()\n",
    "  .map(t => (t._1, t._2.sum))\n",
    "  .collect()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rdd = sc.parallelize([('a',7),('a',2),('b',2)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Filtering"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rdd.filter(lambda x: \"a\" in x).collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sampling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rdd.sample(withReplacement=True, fraction=0.8, seed=81).collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sorting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rdd.sortBy(lambda x: x[1]).collect()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
